# encoding: UTF-8

require 'fileutils'
require 'spreadsheet'

desc "land import"
task "land:import" => :environment do
  book = Spreadsheet.open("doc/lvr_landcsv/A_lvr_land_A.xls")
  sheet = book.worksheet 0

  sheet.each_with_index do |row, index|

    constructed_at = row[14].to_i.to_s
    if constructed_at != '0'
      constructed_at = Date.parse("#{constructed_at[0..-5].to_i + 1911}/#{constructed_at[-4..-3]}/#{constructed_at[-2..-1]}")
    else
      constructed_at = nil
    end

    if row[1].include?("房地")
      LandInformation.create({
        address:                 row[2],
        product_type:            row[11],
        area:                    row[15],
        price:                   row[21],
        price_per_square_meters: row[22],
        constructed_at:          constructed_at
      })
    end
  end
end

desc "land geocoding"
task "land:geocoding" => :environment do
  LandInformation.all.each do |l|
    if (!l.latitude || !l.longitude)
      l.geocode
      l.save
    end

    puts "#{l.latitude}/#{l.longitude} : #{l.address}"
  end
end

desc "land distance"
task "land:distance" => :environment do
  distance_set = []

  book = Spreadsheet.open('doc/tpi.xls')
  sheet = book.worksheet 0

  fd = File.open('doc/distance.txt', 'a+')
  ft = File.open('doc/drive_time.txt', 'a+')

  sheet.each_with_index do |row, index|
    next if index < ENV['index'].to_i

    d = nil

    loop do
      d = GoogleDirections.new(row[2], "台北車站")
      sleep(0.1)
      puts d.status
      break if d.status != 'OVER_QUERY_LIMIT'
    end

    if d.status == 'OK'
      fd.puts("#{index}: #{d.distance_text}")
      ft.puts("#{index}: #{d.drive_time_in_minutes}")
      puts ("#{index}: #{d.distance_text}")
    else
      fd.puts("#{index}: #{d.status}")
      ft.puts("#{index}: #{d.status}")
      puts ("#{index}: #{d.status}")
    end
  end

end

desc "land output"
task "land:output" => :environment do
  fd_o = File.open('doc/distance_output.txt', 'w+')
  ft_o = File.open('doc/drive_time_output.txt', 'w+')

  File.open('doc/distance.txt').each do |fd_line|
    fd_o.puts(fd_line.split(": ")[1].gsub("\n", '').to_s)
  end
  File.open('doc/drive_time.txt').each do |ft_line|
    ft_o.puts(ft_line.split(": ")[1].gsub("\n", '').to_s)
  end
end
