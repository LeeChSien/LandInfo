class LandInformationsController < ApplicationController
  before_action :set_land_information, only: [:show, :edit, :update, :destroy]

  # GET /land_informations
  # GET /land_informations.json
  def index
    @land_informations = LandInformation.all
  end

  # GET /land_informations/1
  # GET /land_informations/1.json
  def show
  end

  # GET /land_informations/new
  def new
    @land_information = LandInformation.new
  end

  # GET /land_informations/1/edit
  def edit
  end

  # POST /land_informations
  # POST /land_informations.json
  def create
    @land_information = LandInformation.new(land_information_params)

    respond_to do |format|
      if @land_information.save
        format.html { redirect_to @land_information, notice: 'Land information was successfully created.' }
        format.json { render :show, status: :created, location: @land_information }
      else
        format.html { render :new }
        format.json { render json: @land_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /land_informations/1
  # PATCH/PUT /land_informations/1.json
  def update
    respond_to do |format|
      if @land_information.update(land_information_params)
        format.html { redirect_to @land_information, notice: 'Land information was successfully updated.' }
        format.json { render :show, status: :ok, location: @land_information }
      else
        format.html { render :edit }
        format.json { render json: @land_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /land_informations/1
  # DELETE /land_informations/1.json
  def destroy
    @land_information.destroy
    respond_to do |format|
      format.html { redirect_to land_informations_url, notice: 'Land information was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_land_information
      @land_information = LandInformation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def land_information_params
      params.require(:land_information).permit(:address, :latitude, :longitude, :price_per_square_meters, :product_type, :price, :area)
    end
end
