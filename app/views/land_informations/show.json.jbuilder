json.extract! @land_information, :id, :address, :latitude, :longitude, :price_per_square_meters, :product_type, :price, :area, :created_at, :updated_at
