json.array!(@land_informations) do |land_information|
  json.extract! land_information, :id, :address, :latitude, :longitude, :price_per_square_meters, :product_type, :price, :area
  json.url land_information_url(land_information, format: :json)
end
