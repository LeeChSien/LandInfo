require 'test_helper'

class LandInformationsControllerTest < ActionController::TestCase
  setup do
    @land_information = land_informations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:land_informations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create land_information" do
    assert_difference('LandInformation.count') do
      post :create, land_information: { address: @land_information.address, area: @land_information.area, latitude: @land_information.latitude, longitude: @land_information.longitude, price: @land_information.price, price_per_square_meters: @land_information.price_per_square_meters, product_type: @land_information.product_type }
    end

    assert_redirected_to land_information_path(assigns(:land_information))
  end

  test "should show land_information" do
    get :show, id: @land_information
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @land_information
    assert_response :success
  end

  test "should update land_information" do
    patch :update, id: @land_information, land_information: { address: @land_information.address, area: @land_information.area, latitude: @land_information.latitude, longitude: @land_information.longitude, price: @land_information.price, price_per_square_meters: @land_information.price_per_square_meters, product_type: @land_information.product_type }
    assert_redirected_to land_information_path(assigns(:land_information))
  end

  test "should destroy land_information" do
    assert_difference('LandInformation.count', -1) do
      delete :destroy, id: @land_information
    end

    assert_redirected_to land_informations_path
  end
end
