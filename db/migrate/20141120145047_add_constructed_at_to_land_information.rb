class AddConstructedAtToLandInformation < ActiveRecord::Migration
  def change
    add_column :land_informations, :constructed_at, :date
  end
end
