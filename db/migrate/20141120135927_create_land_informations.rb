class CreateLandInformations < ActiveRecord::Migration
  def change
    create_table :land_informations do |t|
      t.string :address
      t.float :latitude
      t.float :longitude
      t.integer :price_per_square_meters
      t.string :product_type
      t.integer :price
      t.float :area

      t.timestamps
    end
  end
end
